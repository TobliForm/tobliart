<?php

namespace App\Service;

use App\Entity\Contact;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class ContactService
{
    private $em;
    private $flash;

    public function __construct(EntityManagerInterface $em, FlashBagInterface $flash)
    {
        $this->em = $em;
        $this->flash = $flash;
    }

    public function persistContact(Contact $contact): void
    {
        $contact->setIsSend(false);

        $this->em->persist($contact);
        $this->em->flush();

        $this->flash->add('success', 'Votre message est bien envoyé, merci.');
    }

    public function isSend(Contact $contact): void
    {
        $contact->setIsSend(true);

        $this->em->persist($contact);
        $this->em->flush();
    }
}