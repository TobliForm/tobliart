<?php

namespace App\Service;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class CommentaireService
{
    private $em;
    private $flash;

    public function __construct(EntityManagerInterface $em, FlashBagInterface $flash)
    {
        $this->em = $em;
        $this->flash = $flash;
    }

    public function persistCommentaire(Commentaire $commentaire, Blogpost $blogpost = null, Peinture $peinture = null): void
    {
        $commentaire->setIsPublished(false)
            ->setBlogpost($blogpost)
            ->setPeinture($peinture);
        
        $this->em->persist($commentaire);
        $this->em->flush();

        $this->flash->add('success', 'Votre commentaire est bien envoyé, merci. Il sera publié après validation');
    }

}