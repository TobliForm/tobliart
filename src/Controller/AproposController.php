<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AproposController extends AbstractController
{
    /**
     * @Route("/a-propos", name="app_apropos_index", methods={"GET"})
     */
    public function index(UserRepository $userRepo): Response
    {
        $peintre = $userRepo->getPeintre();

        return $this->render('apropos/index.html.twig', [
            'peintre' => $peintre,
            'current_menu' => 'apropos',
        ]);
    }
}
