<?php

namespace App\Controller;

use App\Entity\Peinture;
use App\Entity\Commentaire;
use App\Form\CommentaireType;
use App\Repository\CommentaireRepository;
use App\Service\CommentaireService;
use App\Repository\PeintureRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PeinturesController extends AbstractController
{
    /**
     * @Route("/realisations", name="app_peintures_realisation", methods={"GET"})
     */
    public function realisation(PeintureRepository $peintureRepo, PaginatorInterface $paginator, Request $request): Response
    {
        $data = $peintureRepo->findBy([], ['id' => 'DESC']);
        $peintures = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );

        return $this->render('peintures/realisation.html.twig', [
            'peintures' => $peintures,
            'current_menu' => 'realisations', 
        ]);
    }

    /**
     * @Route("/realisations/{slug}", name="app_peintures_show", methods={"GET", "POST"})
     */
    public function sow(Request $request, Peinture $peinture, CommentaireService $commentaireService, CommentaireRepository $commentaireRepo): Response
    {
        $commentaires = $commentaireRepo->findCommentaire($peinture);

        $commentaire = new Commentaire();

        $form = $this->createForm(CommentaireType::class, $commentaire);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $commentaire = $form->getData();
            $commentaireService->persistCommentaire($commentaire, null, $peinture);

            return $this->redirectToRoute('app_peintures_show', ['slug' => $peinture->getSlug()]);
        }
        return $this->render('peintures/show.html.twig', [
            'peinture' => $peinture,
            'current_menu' => 'realisations', 
            'form' => $form->createView(),
            'commentaires' => $commentaires,
        ]);
    }
}
