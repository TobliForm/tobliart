<?php

namespace App\Controller;

use App\Repository\BlogpostRepository;
use App\Repository\PeintureRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home", methods={"GET"})
     */
    public function index(PeintureRepository $peintureRepo, BlogpostRepository $blogpostRepo): Response
    {
        $peintures = $peintureRepo->lastPeintures();
        $blogposts = $blogpostRepo->lastBlogpost();

        return $this->render('home/index.html.twig', [
            'peintures' => $peintures,
            'blogposts' => $blogposts
        ]);
    }
}
