<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Repository\PeintureRepository;
use App\Repository\CategorieRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PortfoliosController extends AbstractController
{
    /**
     * @Route("/portfolios", name="app_portfolios_index", methods={"GET"})
     */
    public function index(CategorieRepository $categorieRepo): Response
    {
        return $this->render('portfolios/index.html.twig', [
            'categories' => $categorieRepo->findAll(),
            'current_menu' => 'portfolios',
        ]);
    }

    /**
     * @Route("/portfolios/{slug}", name="app_portfolios_show", methods={"GET"})
     */
    public function show(Categorie $categorie, PeintureRepository $peintureRepo): Response
    {
        return $this->render('portfolios/show.html.twig', [
            'categorie' => $categorie,
            'peintures' => $peintureRepo->findAllPortfolios($categorie),
            'current_menu' => 'portfolios',
        ]);
    }


}
