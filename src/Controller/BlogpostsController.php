<?php

namespace App\Controller;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Form\CommentaireType;
use App\Service\CommentaireService;
use App\Repository\BlogpostRepository;
use App\Repository\CommentaireRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogpostsController extends AbstractController
{
    /**
     * @Route("/actualites", name="app_blogposts_actualite", methods={"GET"})
     */
    public function actualite(BlogpostRepository $blogpostRepo, PaginatorInterface $paginator, Request $request): Response
    {
        $data = $blogpostRepo->findBy([], ['id' => 'DESC']);
        $blogposts = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );

        return $this->render('blogposts/actualite.html.twig', [
            'blogposts' => $blogposts,
            'current_menu' => 'actualites'
        ]);
    }

    /**
     * @Route("/actualites/{slug}", name="app_blogposts_show", methods={"GET", "POST"})
     */
    public function show(Request $request, Blogpost $blogpost, CommentaireService $commentaireService, CommentaireRepository $commentaireRepo): Response
    {
        $commentaires = $commentaireRepo->findCommentaire($blogpost);

        $commentaire = new Commentaire;

        $form = $this->createForm(CommentaireType::class, $commentaire);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $commentaire = $form->getData();
            $commentaireService->persistCommentaire($commentaire, $blogpost, null);
            return $this->redirectToRoute('app_blogposts_show', ['slug' => $blogpost->getSlug()]);
        }
        return $this->render('blogposts/show.html.twig', [
            'blogpost' => $blogpost,
            'current_menu' => 'actualites',
            'form' => $form->createView(),
            'commentaires' => $commentaires,
        ]);
    }
}
