<?php

namespace App\Command;

use App\Repository\ContactRepository;
use App\Repository\UserRepository;
use App\Service\ContactService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class SendContactCommand extends Command
{
    private $contactRepo;
    private $userRepo;
    private $mailer;
    private $contactService;
    protected static $defaultName = 'app:send-contact';

    public function __construct(ContactRepository $contactRepo, UserRepository $userRepo, MailerInterface $mailer, ContactService $contactService)
    {
        $this->contactRepo = $contactRepo;
        $this->userRepo = $userRepo;
        $this->mailer = $mailer;
        $this->contactService = $contactService;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
       $toSend = $this->contactRepo->findBy(['isSend' => false]);
       $address = new Address($this->userRepo->getPeintre()->getEmail(), $this->userRepo->getPeintre()->getFullName());

       foreach($toSend as $mail) {
           $email = (new Email())
                ->from($mail->getEmail())
                ->to($address)
                ->subject('Nouveau message de ' . $mail->getNom())
                ->text($mail->getMessage());

            $this->mailer->send($email);
            $this->contactService->isSend($mail);
       }

        return Command::SUCCESS;
    }
}
