<?php

namespace App\DataFixtures;

use App\Entity\Blogpost;
use App\Entity\Categorie;
use App\Entity\Peinture;
use Faker\Factory;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
       $this->encoder = $encoder; 
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $user = new User;

        $user->setEmail('user@test.com')
             ->setPrenom($faker->firstName())
             ->setNom($faker->lastName())
             ->setTelephone($faker->phoneNumber)
             ->setAPropos($faker->text())
             ->setInstagram('Instagram')
             ->setRoles(['ROLE_PEINTRE']);

        $password = $this->encoder->encodePassword($user, 'password');

        $user->setPassword($password);
        
        $manager->persist($user);

        for ($i=0; $i < 10; $i++) { 
            
            $blogpost = new Blogpost;

            $blogpost->setTitre($faker->words(3, true))
                     ->setContenu($faker->text(350))
                     ->setSlug($faker->slug(3))
                     ->setUser($user);
            
            $manager->persist($blogpost);
        }

        for ($k=0; $k < 5; $k++) { 

            $categorie = new Categorie;

            $categorie->setNom($faker->word())
                      ->setDescription($faker->words(10, true))
                      ->setSlug($faker->slug());

            $manager->persist($categorie);

            for ($j=0; $j < 2; $j++) { 
                
                $peinture = new Peinture;

                $peinture->setNom($faker->words(3, true))
                         ->setLargeur($faker->randomFloat(2, 20, 60))
                         ->setHauteur($faker->randomFloat(2, 20, 60))
                         ->setEnVente($faker->randomElement([true, false]))
                         ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
                         ->setDescription($faker->text())
                         ->setPortfolio($faker->randomElement([true, false]))
                         ->setSlug($faker->slug())
                         ->setFile('placeholder.jpg')
                         ->setPrix($faker->numberBetween(2000000, 3000000, 5000000))
                         ->addCategorie($categorie)
                         ->setUser($user);
                
                         $manager->persist($peinture);
            }
        }

        $manager->flush();
    }
}
