<?php

namespace App\Twig;

use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;
use App\Repository\CategorieRepository;

class AppExtension extends AbstractExtension
{
    private $categorieRepo;

    public function __construct(CategorieRepository $categorieRepo)
    {
        $this->categorieRepo = $categorieRepo;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('categorieNavbar', [$this, 'categorie']),
        ];
    }

    public function categorie()
    {
        return $this->categorieRepo->findAll();
    }
}
