<?php

namespace App\Tests;

use App\Entity\Categorie;
use PHPUnit\Framework\TestCase;

class CategorieUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new Categorie;
        $user->setNom('Nom')
                 ->setDescription('Description')
                 ->setSlug('Slug')
        ;

        $this->assertTrue($user->getNom() === 'Nom');
        $this->assertTrue($user->getDescription() === 'Description');
        $this->assertTrue($user->getSlug() === 'Slug');
    }

    public function testIsFalse(): void
    {
        $user = new Categorie;
        $user->setNom('Nom')
                 ->setDescription('Description')
                 ->setSlug('Slug')
        ;

        $this->assertFalse($user->getNom() === 'False');
        $this->assertFalse($user->getDescription() === 'False');
        $this->assertFalse($user->getSlug() === 'False');
    }

    public function testIsEmpty(): void
    {
        $user = new Categorie;
            
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getDescription());
        $this->assertEmpty($user->getSlug());
    }
}
