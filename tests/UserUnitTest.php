<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User;
        $user->setEmail('true@test.com')
                 ->setPrenom('Prenom')
                 ->setNom('Nom')
                 ->setPassword('Password')
                 ->setAPropos('A propos')
                 ->setInstagram('Instagram')
        ;

        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getPrenom() === 'Prenom');
        $this->assertTrue($user->getNom() === 'Nom');
        $this->assertTrue($user->getPassword() === 'Password');
        $this->assertTrue($user->getAPropos() === 'A propos');
        $this->assertTrue($user->getInstagram() === 'Instagram');
    }

    public function testIsFalse(): void
    {
        $user = new User;
        $user->setEmail('true@test.com')
                 ->setPrenom('Prenom')
                 ->setNom('Nom')
                 ->setPassword('Password')
                 ->setAPropos('A propos')
                 ->setInstagram('Instagram')
        ;

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getPrenom() === 'False');
        $this->assertFalse($user->getNom() === 'False');
        $this->assertFalse($user->getPassword() === 'False');
        $this->assertFalse($user->getAPropos() === 'False');
        $this->assertFalse($user->getInstagram() === 'False');
    }

    public function testIsEmpty(): void
    {
        $user = new User;
            
        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getAPropos());
        $this->assertEmpty($user->getInstagram());
    }
}
