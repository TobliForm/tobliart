<?php

namespace App\Tests;

use App\Entity\Categorie;
use App\Entity\Commentaire;
use App\Entity\User;
use App\Entity\Peinture;
use DateTime;
use PHPUnit\Framework\TestCase;

class PeintureUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $peinture = new Peinture;
        $user = new User;
        $categorie = new Categorie;
        $datetime = new DateTime;

        $peinture->setNom('Nom')
                    ->setLargeur(20, 20)
                    ->setHauteur(20, 20)
                    ->setEnVente(true)
                    ->setPrix(200)
                    ->setDateRealisation($datetime)
                    ->setDescription('Description')
                    ->setPortfolio(true)
                    ->setSlug('Slug')
                    ->setFile('file')
                    ->setUser($user)
                    ->addCategorie($categorie)
        ;

        $this->assertTrue($peinture->getNom() === 'Nom');
        $this->assertTrue($peinture->getLargeur() == 20, 20);
        $this->assertTrue($peinture->getHauteur() == 20, 20);
        $this->assertTrue($peinture->getEnVente() === true);
        $this->assertTrue($peinture->getPrix() == 200);
        $this->assertTrue($peinture->getDateRealisation() === $datetime);
        $this->assertTrue($peinture->getDescription() === 'Description');
        $this->assertTrue($peinture->getPortfolio() === true);
        $this->assertTrue($peinture->getSlug() === 'Slug');
        $this->assertTrue($peinture->getFile() === 'file');
        $this->assertTrue($peinture->getUser() === $user);
        $this->assertContains($categorie, $peinture->getCategorie());
    }

    public function testIsFalse(): void
    {
        $peinture = new Peinture;
        $user = new User;
        $categorie = new Categorie;
        $datetime = new DateTime;

        $peinture->setNom('Nom')
                    ->setLargeur(20, 20)
                    ->setHauteur(20, 20)
                    ->setEnVente(true)
                    ->setPrix(200)
                    ->setDateRealisation($datetime)
                    ->setDescription('Description')
                    ->setPortfolio(true)
                    ->setSlug('Slug')
                    ->setFile('file')
                    ->setUser($user)
                    ->addCategorie($categorie)
        ;

        $this->assertFalse($peinture->getNom() === 'Toto');
        $this->assertFalse($peinture->getLargeur() == 22, 20);
        $this->assertFalse($peinture->getHauteur() == 22, 20);
        $this->assertFalse($peinture->getEnVente() === false);
        $this->assertFalse($peinture->getPrix() == 220);
        $this->assertFalse($peinture->getDateRealisation() === new DateTime());
        $this->assertFalse($peinture->getDescription() === 'toto');
        $this->assertFalse($peinture->getPortfolio() === false);
        $this->assertFalse($peinture->getSlug() === 'toto');
        $this->assertFalse($peinture->getFile() === 'toto');
        $this->assertFalse($peinture->getUser() === new User());
        $this->assertFalse($peinture->getCategorie() === new Categorie());
    }


    public function testIsEmpty(): void
    {
        $peinture = new Peinture;
            
        $this->assertEmpty($peinture->getNom());
        $this->assertEmpty($peinture->getLargeur());
        $this->assertEmpty($peinture->getHauteur());
        $this->assertEmpty($peinture->getEnVente());
        $this->assertEmpty($peinture->getPrix());
        $this->assertEmpty($peinture->getDateRealisation());
        $this->assertEmpty($peinture->getDescription());
        $this->assertEmpty($peinture->getPortfolio());
        $this->assertEmpty($peinture->getSlug());
        $this->assertEmpty($peinture->getFile());
        $this->assertEmpty($peinture->getUser());
        $this->assertEmpty($peinture->getCategorie());
    }
}
