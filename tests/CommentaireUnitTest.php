<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Peinture;
use App\Entity\Commentaire;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $commentaire = new Commentaire;
        $blogpost = new Blogpost;
        $peinture = new Peinture;

        $commentaire->setAuteur('Auteur')
                 ->setContenu('Contenu')
                 ->setEmail('true@true.com')
                 ->setPeinture($peinture)
                 ->setBlogpost($blogpost)
        ;

        $this->assertTrue($commentaire->getAuteur() === 'Auteur');
        $this->assertTrue($commentaire->getContenu() === 'Contenu');
        $this->assertTrue($commentaire->getEmail() === 'true@true.com');
        $this->assertTrue($commentaire->getPeinture() === $peinture);
        $this->assertTrue($commentaire->getBlogpost() === $blogpost);
    }

    public function testIsFalse(): void
    {
        $commentaire = new Commentaire;
        $blogpost = new Blogpost;
        $peinture = new Peinture;

        $commentaire->setAuteur('Auteur')
                 ->setContenu('Contenu')
                 ->setEmail('true@true.com')
                 ->setPeinture($peinture)
                 ->setBlogpost($blogpost)
        ;

        $this->assertFalse($commentaire->getAuteur() === 'false');
        $this->assertFalse($commentaire->getContenu() === 'false');
        $this->assertFalse($commentaire->getEmail() === 'false@true.com');
        $this->assertFalse($commentaire->getPeinture() === new Peinture);
        $this->assertFalse($commentaire->getBlogpost() === new Blogpost);
    }

    public function testIsEmpty(): void
    {
        $commentaire = new Commentaire;

        $this->assertEmpty($commentaire->getAuteur());
        $this->assertEmpty($commentaire->getContenu());
        $this->assertEmpty($commentaire->getEmail());
        $this->assertEmpty($commentaire->getPeinture());
        $this->assertEmpty($commentaire->getBlogpost());
    }
}
