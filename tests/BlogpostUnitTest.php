<?php

namespace App\Tests;

use App\Entity\User;
use App\Entity\Blogpost;
use PHPUnit\Framework\TestCase;

class BlogpostUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $blogPost = new Blogpost;
        $user = new User;

        $blogPost->setTitre('titre')
                 ->setContenu('Contenu')
                 ->setSlug('Slug')
                 ->setUser($user)
        ;

        $this->assertTrue($blogPost->getTitre() === 'titre');
        $this->assertTrue($blogPost->getContenu() === 'Contenu');
        $this->assertTrue($blogPost->getSlug() === 'Slug');
        $this->assertTrue($blogPost->getUser() === $user);
    }

    public function testIsFalse(): void
    {
        $blogPost = new Blogpost;
        $user = new User;

        $blogPost->setTitre('titre')
                 ->setContenu('Contenu')
                 ->setSlug('Slug')
                 ->setUser($user)
        ;
        
        $this->assertFalse($blogPost->getTitre() === 'false');
        $this->assertFalse($blogPost->getContenu() === 'false');
        $this->assertFalse($blogPost->getSlug() === 'false');
        $this->assertFalse($blogPost->getUser() === new User);
    }

    public function testIsEmpty(): void
    {
        $blogPost = new Blogpost;

        $this->assertEmpty($blogPost->getTitre());
        $this->assertEmpty($blogPost->getContenu());
        $this->assertEmpty($blogPost->getSlug());
        $this->assertEmpty($blogPost->getUser());
    }
}
